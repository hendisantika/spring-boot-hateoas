package com.hendisantika.springboothateoas.repository;

import com.hendisantika.springboothateoas.entity.GymMembership;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hateoas
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-13
 * Time: 05:13
 * To change this template use File | Settings | File Templates.
 */
public interface GymMembershipRepository extends JpaRepository<GymMembership, Long> {
}
