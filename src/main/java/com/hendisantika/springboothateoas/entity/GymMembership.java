package com.hendisantika.springboothateoas.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hateoas
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-13
 * Time: 05:11
 * To change this template use File | Settings | File Templates.
 */
@Getter
@Setter
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "memberships")
public class GymMembership {

    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Person owner;

    private String name;

    private long cost;

    public GymMembership(final Person owner, final String name, final long cost) {
        this.owner = owner;
        this.name = name;
        this.cost = cost;
    }
}