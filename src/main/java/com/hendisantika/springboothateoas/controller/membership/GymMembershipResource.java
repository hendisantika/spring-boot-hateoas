package com.hendisantika.springboothateoas.controller.membership;

import com.hendisantika.springboothateoas.controller.person.PersonController;
import com.hendisantika.springboothateoas.entity.GymMembership;
import lombok.Getter;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hateoas
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-13
 * Time: 05:22
 * To change this template use File | Settings | File Templates.
 */
@Getter
public class GymMembershipResource extends ResourceSupport {

    private final GymMembership gymMembership;

    public GymMembershipResource(final GymMembership gymMembership) {
        this.gymMembership = gymMembership;
        final long membershipId = gymMembership.getId();
        final long personId = gymMembership.getOwner().getId();
        add(new Link(String.valueOf(membershipId), "membership-id"));
        add(linkTo(methodOn(GymMembershipController.class).all(personId)).withRel("memberships"));
        add(linkTo(methodOn(PersonController.class).get(personId)).withRel("owner"));
        add(linkTo(methodOn(GymMembershipController.class).get(personId, membershipId)).withSelfRel());
    }
}