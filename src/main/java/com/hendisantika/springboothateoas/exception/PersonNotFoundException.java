package com.hendisantika.springboothateoas.exception;

import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hateoas
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-13
 * Time: 05:15
 * To change this template use File | Settings | File Templates.
 */
@Getter
public class PersonNotFoundException extends RuntimeException {

    private final Long id;

    public PersonNotFoundException(final long id) {
        super("Person could not be found with id: " + id);
        this.id = id;
    }

}