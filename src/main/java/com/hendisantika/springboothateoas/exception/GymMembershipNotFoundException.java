package com.hendisantika.springboothateoas.exception;

import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hateoas
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-13
 * Time: 05:14
 * To change this template use File | Settings | File Templates.
 */
@Getter
public class GymMembershipNotFoundException extends RuntimeException {

    private final long id;

    public GymMembershipNotFoundException(final long id) {
        super("GymMembership could not be found with id: " + id);
        this.id = id;
    }

}